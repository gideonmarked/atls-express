<?php namespace Grinkomeda\TravelAgent\Components;

use Cms\Classes\ComponentBase;

class RegistrationForm extends ComponentBase
{

    public function componentDetails()
    {
        return [
            'name'        => 'RegistrationForm Component',
            'description' => 'No description provided yet...'
        ];
    }

    public function defineProperties()
    {
        return [];
    }

}