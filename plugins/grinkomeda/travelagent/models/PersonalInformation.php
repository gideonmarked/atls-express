<?php namespace Grinkomeda\TravelAgent\Models;

use Model;

/**
 * PersonalInformation Model
 */
class PersonalInformation extends Model
{

    /**
     * @var string The database table used by the model.
     */
    public $table = 'grinkomeda_travelagent_personal_informations';

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = [];

    /**
     * @var array Relations
     */
    public $hasOne = [];
    public $hasMany = [];
    public $belongsTo = [];
    public $belongsToMany = [];
    public $morphTo = [];
    public $morphOne = [];
    public $morphMany = [];
    public $attachOne = [];
    public $attachMany = [];

    public function beforeCreate()
    {
        $this->member_id = $this->createMemberID();
    }

    private function createMemberID()
    {
        $member_id = '';
        $member_id .= strtoupper(substr($this->given_name, 0, 1));
        $member_id .= strtoupper(substr($this->middle_name, 0, 1));
        $member_id .= strtoupper(substr($this->last_name, 0, 1));
        $member_id .= date('m') . '';
        $member_id .= date('d') . '';
        $member_id .= date('Y') . '';

        $random_string = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789';
        for ($i=0; $i < 3; $i++) { 
            $member_id .= substr($random_string, rand(0, strlen($random_string) - 1), 1);
        }

        return $member_id;
    }

}