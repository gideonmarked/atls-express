<?php namespace Grinkomeda\TravelAgent\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreatePersonalInformationsTable extends Migration
{
    public function up()
    {
        Schema::create('grinkomeda_travelagent_personal_informations', function(Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('username');
            $table->string('given_name');
            $table->string('middle_name');
            $table->string('last_name');
            $table->string('email');
            $table->string('address');
            $table->date('birthday');
            $table->string('member_id');
            $table->string('sponsor_id');
            $table->string('sponsor_name');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('grinkomeda_travelagent_personal_informations');
    }
}
