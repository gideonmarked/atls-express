<?php namespace Grinkomeda\TravelAgent\Controllers;

use BackendMenu;
use Backend\Classes\Controller;

/**
 * Personal Informations Back-end Controller
 */
class PersonalInformations extends Controller
{
    public $implement = [
        'Backend.Behaviors.FormController',
        'Backend.Behaviors.ListController'
    ];

    public $formConfig = 'config_form.yaml';
    public $listConfig = 'config_list.yaml';

    public function __construct()
    {
        parent::__construct();

        BackendMenu::setContext('Grinkomeda.TravelAgent', 'travelagent', 'personalinformations');
    }
}