<?php namespace Grinkomeda\TravelAgent;

use Backend;
use System\Classes\PluginBase;

/**
 * TravelAgent Plugin Information File
 */
class Plugin extends PluginBase
{

    /**
     * Returns information about this plugin.
     *
     * @return array
     */
    public function pluginDetails()
    {
        return [
            'name'        => 'TravelAgent',
            'description' => 'No description provided yet...',
            'author'      => 'Grinkomeda',
            'icon'        => 'icon-leaf'
        ];
    }

    /**
     * Register method, called when the plugin is first registered.
     *
     * @return void
     */
    public function register()
    {

    }

    /**
     * Boot method, called right before the request route.
     *
     * @return array
     */
    public function boot()
    {

    }

    /**
     * Registers any front-end components implemented in this plugin.
     *
     * @return array
     */
    public function registerComponents()
    {
        return [
            'Grinkomeda\TravelAgent\Components\RegistrationForm' => 'registrationForm',
        ];
    }

    /**
     * Registers any back-end permissions used by this plugin.
     *
     * @return array
     */
    public function registerPermissions()
    {
        return [
            'grinkomeda.travelagent.some_permission' => [
                'tab' => 'TravelAgent',
                'label' => 'Some permission'
            ],
        ];
    }

    /**
     * Registers back-end navigation items for this plugin.
     *
     * @return array
     */
    public function registerNavigation()
    {
        return [
            'travelagent' => [
                'label'       => 'ATLS Express',
                'url'         => Backend::url('grinkomeda/travelagent/personalinformations'),
                'icon'        => 'icon-leaf',
                'permissions' => ['grinkomeda.travelagent.*'],
                'order'       => 500,
                'sideMenu' => [
                    'personalinformation' => [
                        'label'       => 'Personal Information',
                        'icon'        => 'icon-copy',
                        'url'         => Backend::url('grinkomeda/travelagent/personalinformations'),
                        'permissions' => ['grinkomeda.travelagent.*'],
                    ],
                ]
            ],


        ];
    }

}
